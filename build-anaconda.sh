#!/bin/bash

set -xeuo pipefail
module purge
module load ./fenics-module

conda create -p "${FENICS_PREFIX}" -m -y python=3.6 git cmake boost eigen
#source activate "${FENICS_PREFIX}"
