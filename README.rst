Install FEniCS on the Abel cluster using Intel compilers
========================================================

Install scripts for FEniCS on the Abel cluster at the University of Oslo that
are inspired / copied from `Jack Hale's uni.lu FEniCS cluster recipe
<https://bitbucket.org/unilucompmech/fenics-gaia-cluster>`_.

Intel compilers, Intel MPI (MPICH derivative) and MKL are used. Please
note that these choices are not based on testing, it could be that gcc,
OpenMPI and OpenBLAS would be just as fast for your FEniCS program.
PETSc is installed as the LA backend with support for HYPRE and MUMPS ++

To install FEniCS, download the scripts in this repository and run::

  sh build-all.sh

The full build takes approximately 4.5 hours when run on the Abel login node.

You can also build the individual parts by themselves using the ``build-*.sh`` 
scripts. The ``fenics-module`` file is a modulefile that you can load by
running::

  module load ./fenics-module

There is some configuration in the top of the module file that you may
want to change before installing. After installation you can load the module
file to make Python 3 accessible with a working installation of FEniCS.

.. note::
    The scripts build dolfin sucessfully and all seem to work just fine. Some
    manual intervention is still needed towards the end of the build, see the
    updates below for details (2018-01-18). These problems should probably be
    fixed, but I have spent way to much time on this already, so don't hold
    your breath waiting for a fix...


About
-----

These scripts are meant to be used as a starting point for your own
experimentation. No support is given and the scripts will probably fail if
you run them very much later than the last commit to this git repo due to
some DOLFIN or PETSc software dependency changing in the meanwhile.

The ``*.sh`` scripts are licensed under the MIT license, following the license of
the build script from uni.lu Computational Mechanics which this is based on.
The ``fenics-pull`` and ``fenics-build`` are modified version of the `FEniCS
Docker dev-env setup scripts <https://bitbucket.org/fenics-project/docker/src/65578e3ac205cd733895b213213253b23edfcd04/dockerfiles/dev-env/bin/?at=master>`_.
The license for these appears to be the 2-clause BSD license and the same
applies to these scripts in this repository.

These scripts have been modified to work on the Abel cluster by Tormod Landet
with some email support from UiO USIT.

Update 2018-07-14
-----------------

Rework scripts to build with latest version of PETSc and use Anaconda for
some basic packages (cmake, boost, git) to improve build time. Seems to
build and import from python 3.6, but not tested much yet.

Contains some hacks for building h5py with intel mpiicc instead of gcc
which for some reason snuck back in with the Anaconda python packages.

Update 2018-01-18
-----------------

The install seems to work! Extremely limited testing so far, but
``import dolfin`` no longer gives an error, which is a good start!

Added CMake and Boost to the build in order to get newer versions and avoid
linking problems in DOLFIN. The build now writes a ``build-all.log`` file with
timing of all commands to track the build progress. The whole build takes
around 4-5 hours when using 1 CPU.

Had to disable generation of form files for demos, test and bench directories
since it hangs at 100% CPU without finishing. This means that
``dolfin/CMakeLists.txt`` must be edited after running ``fenics-pull`` and
before running ``fenics-build``. Try without this hack first, it seems to be
random whether ``ffc`` hangs or not.

To get the pybind11 JIT to work a modification was necessary to the dolfin
pkgconfig file, ``${FENICS_PREFIX}/lib/pkgconfig/dolfin.pc`` which for some
reason had a reference to the 2017.1 version of the Intel MPI libraries. When
this reference was removed the JIT seemed to work.


Update 2018-01-16
-----------------

Compiling! After adding some environment cleanup code to the module file it now
seems like the recipe is working almost as intended. There was a problem having
multiple Intel compiler include directories in the CPATH etc at the same time.

The ``fenics-module`` module file is now a bit more hacky than what is ideal,
but it works both for configuring, building and running FEniCS on Abel, which
is somewhat redeeming.

There is still a boost linking issue that needs to be resolved


Update 2018-01-11
-----------------

Not working yet, but defining ``PETSC_SKIP_COMPLEX`` makes the Intel compiler 
able to include ``petscmath.h`` (included almost whenever PETSc is used)
without running into problems with complex numbers. For some reason the 
PETSc ``make test`` script works regardless of this flag, but DOLFIN's test
for linking to PETSc would fail with an error related to ``PetscComplex``::

  petsc_test_lib.cpp(2):
  .../include/petscmath.h(422): 
  error: expected a ")"
       return PetscComplex(x,y)
                            ^

**Update:** cleaning the paths of the older Intel compiler directories seem
to have fixed all the ``complex`` problems, so ``PETSC_SKIP_COMPLEX`` is no
longer needed, see the update from 2018-01-16

