#!/bin/bash

set -xeuo pipefail
module purge
module load ./fenics-module

# Some final cleanups
#ln -s $FENICS_PREFIX/lib64/pkgconfig/dolfin.pc $FENICS_PREFIX/lib/pkgconfig/dolfin.pc
mv $FENICS_PREFIX/lib64/*dolfin* $FENICS_PREFIX/lib/
mv $FENICS_PREFIX/lib64/pkgconfig/*dolfin* $FENICS_PREFIX/lib/pkgconfig/
