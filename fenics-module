#%Module1.0
module-whatis	"FEniCS module for use on Abel"

###############################################################################
# Configure the installation (before building)
set             tag                 2018.1.0.dev0+master-2018-07-16
set             prefix              $env(HOME)/nobackup/install/fenics-$tag
set             build_dir           $env(HOME)/nobackup/build/fenics-$tag
set             python_exe          python3.6
set             build_threads       1

###############################################################################
# Setup the environment to build and run FEniCS w/ PETSc and friends

# Configuration exports
setenv          FENICS_PREFIX         $prefix
setenv          FENICS_SRC_DIR        $build_dir
setenv          FENICS_BUILD_THREADS  $build_threads
setenv          FENICS_BUILD_DIR      $build_dir
setenv          FENICS_PYTHON         $python_exe
setenv          PETSC_DIR             $prefix
setenv          BOOST_ROOT            $prefix
setenv          BOOST_DIR             $prefix
setenv          BOOST_INCLUDEDIR      $prefix/include
setenv          BOOST_LIBRARYDIR      $prefix/lib
setenv          Boost_NO_SYSTEM_PATHS ON
setenv          PYBIND11_DIR          $prefix
setenv          HDF5_DIR              $prefix

# Environment setup
# Notes:
#   - Intel compiler mimics the version of gcc that is
#     available, make sure it is recent)
module  load    Anaconda3/5.1.0
module  load    gcc/7.2.0
module  load    intel/2018.3

# Compiler setup
set     flags         "-O2 -xAVX -mavx -DEIGEN_USE_MKL_ALL"
setenv  CFLAGS        "$flags"
setenv  FCFLAGS       "$flags"
setenv  CXXFLAGS      "$flags"
setenv  F90FLAGS      "$flags"
setenv  CC            mpicc
setenv  CXX           mpicxx
setenv  FC            mpifc
setenv  F77           mpifc
setenv  F90           mpifc
setenv  I_MPI_CC      icc
setenv  I_MPI_CXX     icpc
setenv  I_MPI_FC      ifort
setenv  I_MPI_F90     ifort
setenv  I_MPI_F77     ifort

# Path extensions
prepend-path  PATH                  $prefix/bin
prepend-path  LD_LIBRARY_PATH       $prefix/lib
prepend-path  CPATH                 $prefix/include
prepend-path  CXXPATH               $prefix/include
prepend-path  C_INCLUDE_PATH        $prefix/include
prepend-path  CPLUS_INCLUDE_PATH    $prefix/include
prepend-path  PKG_CONFIG_PATH       $prefix/lib/pkgconfig
prepend-path  MANPATH               $prefix/share/man
prepend-path  PYTHONPATH            $prefix/lib/$python_exe/site-packages
