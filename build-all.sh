#!/bin/bash

# IMPORTANT: run this script with bash, not sh, otherwise the module commands
#     will fail and the script will exit with an error about unset variables

set -xeuo pipefail

logfile="build-all.log"
rm -f $logfile

function run_build_step() {
    sname="$1"
    printf "Running build step ${sname} at $(date):\n" | tee -a $logfile
    { time sh $sname ; } |& tee -a $logfile
    printf "Done with step ${sname} at $(date)\n\n\n\n" | tee -a $logfile
}

# Rough build sequence:
# - Start with anaconda to get git, cmake and boost
# - Next compile basic stuff in order of basicness
# - End with python modules that wrap the basics

# Build steps to run                        Time to build (slow due to file IO)
run_build_step build-anaconda.sh          # Takes approximately   5 minutes
run_build_step build-petsc.sh             # Takes approximately 145 minutes
run_build_step build-python-modules.sh    # Takes approximately  10 minutes
run_build_step fenics-pull                # Takes approximately   2 minutes
run_build_step fenics-build               # Takes approximately  90 minutes
run_build_step build-final.sh             # Takes approximately   0 minutes

diff=$SECONDS # SECONDS is always time from start of script in bash
echo "In total $(($diff / 3600)) hours, $((($diff / 60) % 60)) minutes and $(($diff % 60)) seconds elapsed." | tee -a $logfile
