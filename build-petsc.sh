#!/bin/bash

set -xeuo pipefail
module purge
module load ./fenics-module

unset PETSC_DIR
unset SLEPC_DIR

VERSION="3.9.2"
URL_BASE=http://ftp.mcs.anl.gov/pub/petsc/release-snapshots

mkdir -p $FENICS_BUILD_DIR

cd ${FENICS_BUILD_DIR}
wget --read-timeout=10 -nc ${URL_BASE}/petsc-lite-${VERSION}.tar.gz
tar -xf petsc-lite-${VERSION}.tar.gz

cd petsc-${VERSION}

# Configure PETSc
# The F77, F90 and FCFLAGS variables are just there to silence some warnings
python2 ./configure \
               --with-cc=mpicc \
               --with-fc=mpifc F77=mpifc F90=mpifc \
               --with-cxx=mpigxx \
               --with-mpiexec=mpiexec \
               --COPTFLAGS="${CFLAGS}" \
               --CXXOPTFLAGS="${CXXFLAGS}" \
               --FOPTFLAGS="${FFLAGS}" FCFLAGS="${FFLAGS}" F90FLAGS="${F90FLAGS}" \
               --CFLAGS="${CFLAGS}" \
               --FFLAGS="${FFLAGS}" \
               --CXXFLAGS="${CXXFLAGS}" \
               --LDFLAGS="${LDFLAGS}" \
               --with-blaslapack-dir=$MKLROOT \
               --download-metis \
               --download-parmetis \
               --download-suitesparse \
               --download-scalapack \
               --download-hypre \
               --download-mumps \
               --download-ml \
               --download-hdf5 \
               --with-debugging=0 \
               --with-shared-libraries \
               --prefix=${FENICS_PREFIX}
make MAKE_NP=${FENICS_BUILD_THREADS}
make install
