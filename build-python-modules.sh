#!/bin/bash

set -xeuo pipefail
module purge
module load ./fenics-module


###############################################################################
# Install PyYAML with conda to avoid headache
source activate $FENICS_PREFIX
conda install -y pyyaml
source deactivate

###############################################################################
# Super evil hack to work around setup.py build_ext wanting to use gcc and
# ignoring the CC environmental variable ... ARGHHHH .... :-(

# Thes should not exist, but just to be sure ...
rm -f ${FENICS_PREFIX}/bin/gcc
rm -f ${FENICS_PREFIX}/bin/mpiicc

# Trick setup.py to think it is running gcc ...
export CFLAGS="$CFLAGS -gcc-name=$(which gcc)"
ln -s $(which mpicc) "${FENICS_PREFIX}/bin/gcc"
ln -s $(which mpiicc) "${FENICS_PREFIX}/bin/mpiicc"

# Install the modules that need compilation and insist on gcc
pip install -v --no-binary=h5py h5py --ignore-installed

# Undo super evil hack
rm ${FENICS_PREFIX}/bin/gcc
rm ${FENICS_PREFIX}/bin/mpiicc

###############################################################################
# pip install various modules

PIP_INSTALL="pip install --timeout 100"

${PIP_INSTALL} pip cython
${PIP_INSTALL} pkgconfig
${PIP_INSTALL} numpy matplotlib
${PIP_INSTALL} numexpr ply six sympy pandas ipython ipyparallel
${PIP_INSTALL} mpi4py petsc4py # slepc4py
${PIP_INSTALL} meshio pytest
${PIP_INSTALL} yschema raschii

###############################################################################
# Install pybind11

cd ${FENICS_BUILD_DIR}
PYBINDVER="2.2.3"
rm -f pybind.tgz
wget -nc https://github.com/pybind/pybind11/archive/v${PYBINDVER}.tar.gz -O pybind.tgz
tar -xf pybind.tgz
cd pybind11-${PYBINDVER}
rm -rf build-dir
mkdir build-dir
cd build-dir
cmake -DPYBIND11_TEST=off -DCMAKE_INSTALL_PREFIX=${FENICS_PREFIX} ../
make install
